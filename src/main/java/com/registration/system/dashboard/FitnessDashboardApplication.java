package com.registration.system.dashboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FitnessDashboardApplication
{
    
    public static void main( String[] args )
    {
        SpringApplication.run( FitnessDashboardApplication.class, args );
    }
}
