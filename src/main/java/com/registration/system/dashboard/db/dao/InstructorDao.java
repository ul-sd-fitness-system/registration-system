package com.registration.system.dashboard.db.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.registration.system.dashboard.db.domain.Instructor;

public interface InstructorDao extends JpaRepository<Instructor, Integer> {

}