package com.registration.system.dashboard.db.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.registration.system.dashboard.db.domain.FreeBooking;

public interface FreeBookingDao extends JpaRepository<FreeBooking, Integer>{
	

	
}
