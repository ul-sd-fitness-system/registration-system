package com.registration.system.dashboard.db.domain;

public abstract class BookedClass {
	public BookedClass(String type) {
		this.setType(type);
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void notifySub() {
		
	}
	private String type;

}
