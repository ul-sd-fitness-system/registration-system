package com.registration.system.dashboard.db.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.registration.system.dashboard.db.domain.Location;

public interface LocationDao extends JpaRepository<Location, Integer>{
	

}
