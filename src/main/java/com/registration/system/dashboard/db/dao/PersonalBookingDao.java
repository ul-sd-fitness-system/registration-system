package com.registration.system.dashboard.db.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.registration.system.dashboard.db.domain.PersonalBooking;

public interface PersonalBookingDao extends JpaRepository<PersonalBooking, Integer>{
	
	
}
