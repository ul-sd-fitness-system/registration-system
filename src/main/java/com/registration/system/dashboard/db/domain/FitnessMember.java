package com.registration.system.dashboard.db.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "fitness_member")
public class FitnessMember {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "fitness_member_id")
	private Integer fitnessMemberId;

	@Column(name = "name")
	private String name;

	@Column(name = "dob")
	private String dob;

	@Column(name = "email")
	private String email;

	@Column(name = "phone")
	private String phone;

	@Column(name = "type")
	private String type;

	@Column(name = "memberType")
	private String memberType;

	public Integer getFitnessMemberId() {
		return fitnessMemberId;
	}

	public void setFitnessMemberId(Integer fitnessMemberId) {
		this.fitnessMemberId = fitnessMemberId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

}
