package com.registration.system.dashboard.db.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.registration.system.dashboard.db.domain.FitnessMember;


public interface FitnessMemberDao extends JpaRepository<FitnessMember, Integer> {
	
	public FitnessMember findByEmail( String email );

}
