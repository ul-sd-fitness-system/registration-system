package com.registration.system.dashboard.db.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.registration.system.dashboard.db.domain.Facility;

public interface FacilityDao extends JpaRepository<Facility, Integer> {


}