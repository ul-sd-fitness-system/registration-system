package com.registration.system.dashboard.db.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.registration.system.dashboard.db.domain.MemberBooking;

public interface MemberBookingDao extends JpaRepository<MemberBooking, Integer>{
	
	
}
