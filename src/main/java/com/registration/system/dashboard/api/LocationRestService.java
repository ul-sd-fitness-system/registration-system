package com.registration.system.dashboard.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.registration.system.dashboard.api.common.ApiResponse;
import com.registration.system.dashboard.api.common.ApiResponseBuilder;
import com.registration.system.dashboard.api.ui.LocationUI;
import com.registration.system.dashboard.service.LocationService;

@RestController
@RequestMapping("location")
public class LocationRestService {

	@Autowired
	private LocationService locationService;


	@PostMapping("/addLocation")
	public ApiResponse adminAddLocation(@RequestBody LocationUI locationUI) {

		locationService.addLocation(locationUI);
		return ApiResponseBuilder.success().build();
	}


}