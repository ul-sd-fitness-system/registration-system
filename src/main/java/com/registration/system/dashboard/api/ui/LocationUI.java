package com.registration.system.dashboard.api.ui;

public class LocationUI {

	private String locationName;

	private String locationAddress;

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getLocationAddress() {
		return locationAddress;
	}

	public void setLocationAddress(String locationAddress) {
		this.locationAddress = locationAddress;
	}

	@Override
	public String toString() {
		return "LocationUI [locationName=" + locationName + ", locationAddress=" + locationAddress + "]";
	}
	

}
