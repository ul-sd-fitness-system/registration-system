package com.registration.system.dashboard.api.ui;

public class FacilityUI {
	
	private String facilityType;

	public String getFacilityType() {
		return facilityType;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	@Override
	public String toString() {
		return "FacilityUI [facilityType=" + facilityType + "]";
	}
	
}
