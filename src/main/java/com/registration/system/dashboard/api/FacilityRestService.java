package com.registration.system.dashboard.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.registration.system.dashboard.api.common.ApiResponse;
import com.registration.system.dashboard.api.common.ApiResponseBuilder;
import com.registration.system.dashboard.api.ui.CancelUI;
import com.registration.system.dashboard.api.ui.FacilityUI;
import com.registration.system.dashboard.service.FacilityService;
import com.registration.system.dashboard.service.InvokerCommandService;

@RestController
@RequestMapping("facility")
public class FacilityRestService {
	
	InvokerCommandService command;

	@Autowired
	private FacilityService facilityService;

	@PostMapping("/addFacility")
	public ApiResponse adminAddFacility(@RequestBody FacilityUI facilityUI) {

		facilityService.addFacility(facilityUI);
		return ApiResponseBuilder.success().build();
	}
	
	@PostMapping("/deleteFacility")
	public ApiResponse adminDeleteFacility(@RequestBody CancelUI cancelUI) {
		
		command.deleteButton();
		return ApiResponseBuilder.success().build();
	}
	
}
