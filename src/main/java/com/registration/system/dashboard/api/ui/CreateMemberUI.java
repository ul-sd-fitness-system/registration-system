package com.registration.system.dashboard.api.ui;

import java.io.Serializable;

public class CreateMemberUI implements Serializable {

	private static final long serialVersionUID = 4238848695428903444L;
	
	private String name;
	
	private String dob;
	
	private String email;
	
	private String phone;
	
	private String type;
	
	private String memberType;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "CreateMemberUI [name=" + name + ", dob=" + dob + ", email=" + email + ", phone=" + phone + ", type="
				+ type + ", memberType=" + memberType + "]";
	}
	
	}
