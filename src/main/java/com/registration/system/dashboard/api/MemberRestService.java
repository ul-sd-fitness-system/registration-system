package com.registration.system.dashboard.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.registration.system.dashboard.api.common.ApiResponse;
import com.registration.system.dashboard.api.common.ApiResponseBuilder;
import com.registration.system.dashboard.api.ui.CreateMemberUI;
import com.registration.system.dashboard.service.MemberService;

@RestController
@RequestMapping("member")
public class MemberRestService {

	@Autowired
	private MemberService memberService;

	@PostMapping("/enroll")
	public ApiResponse createOnlineApplication(@RequestBody CreateMemberUI createMemberUI) {

		memberService.createMember(createMemberUI);
		return ApiResponseBuilder.success().build();
	}

}
