package com.registration.system.dashboard.api.ui;

public class InstructorUI {
	
	private String instructorName;

	private String email;
	
	private String phone;
	
	private String facilityType;

	public String getInstructorName() {
		return instructorName;
	}

	public void setInstructorName(String instructorName) {
		this.instructorName = instructorName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFacilityType() {
		return facilityType;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	@Override
	public String toString() {
		return "InstructorUI [instructorName=" + instructorName + ", email=" + email + ", phone=" + phone
				+ ", facilityType=" + facilityType + "]";
	}
	

}
