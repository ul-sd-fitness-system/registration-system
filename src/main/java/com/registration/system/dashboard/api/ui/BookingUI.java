package com.registration.system.dashboard.api.ui;

public class BookingUI {
	
	private String name;
	
	private String email;
	
	private String classType;
	
	private String date;
	
	private String location;
	
	private String instructor;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getClassType() {
		return classType;
	}

	public void setClassType(String classType) {
		this.classType = classType;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	public String getInstructor() {
		return instructor;
	}

	public void setInstructor(String instructor) {
		this.instructor = instructor;
	}

	@Override
	public String toString() {
		return "FreeBookingUI [name=" + name + ", email=" + email + ", classType=" + classType + ", date=" + date
				+ ", location=" + location + ", instructor=" + instructor + "]";
	}

	

}
