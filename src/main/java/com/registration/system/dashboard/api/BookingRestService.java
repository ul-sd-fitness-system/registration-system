package com.registration.system.dashboard.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.registration.system.dashboard.api.common.ApiResponse;
import com.registration.system.dashboard.api.common.ApiResponseBuilder;
import com.registration.system.dashboard.api.ui.BookingUI;
import com.registration.system.dashboard.service.BookingService;


@RestController
@RequestMapping("booking")
public class BookingRestService {

	@Autowired
	private BookingService bookingService;

	@PostMapping("/book")
	public ApiResponse bookClasses(@RequestBody BookingUI bookingUI) {

		bookingService.bookingDone(bookingUI);
		return ApiResponseBuilder.success().build();
	}

}
