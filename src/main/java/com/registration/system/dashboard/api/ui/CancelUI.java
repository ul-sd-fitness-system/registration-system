package com.registration.system.dashboard.api.ui;

public class CancelUI {
	private int facilityID;

	public int getFacilityID() {
		return facilityID;
	}

	public void setFacilityID(int facilityID) {
		this.facilityID = facilityID;
	}
	
	@Override
	public String toString() {
		return "CancelUI [facilityID=" + facilityID + "]";
	}
}
