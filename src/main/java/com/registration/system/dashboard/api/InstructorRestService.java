package com.registration.system.dashboard.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.registration.system.dashboard.api.common.ApiResponse;
import com.registration.system.dashboard.api.common.ApiResponseBuilder;
import com.registration.system.dashboard.api.ui.InstructorUI;
import com.registration.system.dashboard.service.InstructorService;

@RestController
@RequestMapping("instructor")
public class InstructorRestService {

	@Autowired
	private InstructorService instructorService;

	@PostMapping("/addInstructor")
	public ApiResponse adminAddFacility(@RequestBody InstructorUI instructorUI) {

		instructorService.addInstructor(instructorUI);
		return ApiResponseBuilder.success().build();
	}

}
