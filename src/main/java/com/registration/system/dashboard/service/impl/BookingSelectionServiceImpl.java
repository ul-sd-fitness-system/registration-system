package com.registration.system.dashboard.service.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.registration.system.dashboard.api.ui.BookingUI;
import com.registration.system.dashboard.db.dao.FitnessMemberDao;
import com.registration.system.dashboard.db.dao.FreeBookingDao;
import com.registration.system.dashboard.db.dao.MemberBookingDao;
import com.registration.system.dashboard.db.dao.PersonalBookingDao;
import com.registration.system.dashboard.db.domain.FitnessMember;
import com.registration.system.dashboard.service.BookedClass;
import com.registration.system.dashboard.service.BookingService;
@Service
public class BookingSelectionServiceImpl implements BookingService {
	
	@Autowired
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	FitnessMemberDao fitnessMemberDao;
	
	@Autowired
	private FreeBookingDao freeBookingDao;
	
	@Autowired
	private PersonalBookingDao personalBookingDao;
	
	@Autowired
	private MemberBookingDao memberBookingDao;
	
	@Autowired
	private ReminderServiceImpl reminderServiceImpl;
	
	@SuppressWarnings({ "unused", "unlikely-arg-type" })
	@Override
	public void bookingDone(BookingUI bookingUI) {
		BookedClass bclass = null;
		String inputEmail = bookingUI.getEmail();
	
		FitnessMember fitnessMember = fitnessMemberDao.findByEmail(inputEmail);
		if (fitnessMember == null ) {
			FreeBookingServiceImpl freeBookingServiceImpl = new FreeBookingServiceImpl();
			freeBookingServiceImpl.signup(bookingUI);
			freeBookingDao.save(freeBookingServiceImpl.book());
			reminderServiceImpl.subscribe(freeBookingServiceImpl.book());
		}
		else {
		String memberType = fitnessMember.getMemberType();
		System.out.println(fitnessMember.getEmail());
		
		if (fitnessMember != null && memberType.equals("2") ) {
			PersonalBookingServiceImpl personalBookingServiceImpl = new PersonalBookingServiceImpl();
			personalBookingDao.save(personalBookingServiceImpl.bookDone(bookingUI));
			reminderServiceImpl.subscribe(personalBookingServiceImpl.bookDone(bookingUI));
		}
		
		if (fitnessMember != null && memberType.equals("1") ) {
			MemberBookingServiceImpl memberBookingServiceImpl = new MemberBookingServiceImpl();
			memberBookingDao.save(memberBookingServiceImpl.bookDone(bookingUI));
			reminderServiceImpl.subscribe(memberBookingServiceImpl.bookDone(bookingUI));
		}
		
		}
		
		reminderServiceImpl.reminder();
		
	}
	

}
