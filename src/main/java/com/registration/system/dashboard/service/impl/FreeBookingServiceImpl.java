package com.registration.system.dashboard.service.impl;

import com.registration.system.dashboard.api.ui.BookingUI;
import com.registration.system.dashboard.db.domain.FreeBooking;
import com.registration.system.dashboard.service.BookedClass;



public class FreeBookingServiceImpl extends BookedClass{
	

	
	public FreeBookingServiceImpl() {
		super("free");
		// TODO Auto-generated constructor stub
	}
	FreeBooking booking = new FreeBooking();
	
	public void signup(BookingUI freeBookingUI) {
		
		booking.setName(freeBookingUI.getName());
		booking.setEmail(freeBookingUI.getEmail());
		booking.setLocation(freeBookingUI.getLocation());
		booking.setClassType(freeBookingUI.getClassType());
		booking.setDate(freeBookingUI.getDate());
	
	}

	public FreeBooking book() {
		return booking;	
	}
	public void notifySub() {
		System.out.println("Hi");
		
	}
	
	
}
