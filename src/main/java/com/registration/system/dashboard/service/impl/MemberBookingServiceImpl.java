package com.registration.system.dashboard.service.impl;

import com.registration.system.dashboard.api.ui.BookingUI;
import com.registration.system.dashboard.db.domain.MemberBooking;
import com.registration.system.dashboard.service.BookedClass;

public class MemberBookingServiceImpl extends BookedClass{
	
	public MemberBookingServiceImpl() {
		super("member");
		// TODO Auto-generated constructor stub
	}


	public MemberBooking bookDone(BookingUI freeBookingUI) {
		MemberBooking booking = new MemberBooking();
		booking.setName(freeBookingUI.getName());
		booking.setEmail(freeBookingUI.getEmail());
		booking.setLocation(freeBookingUI.getLocation());
		booking.setClassType(freeBookingUI.getClassType());
		booking.setDate(freeBookingUI.getDate());
		return booking;	
	}
	

	public void notifySub() {
		System.out.println("Hi");
		
	}
	

}
