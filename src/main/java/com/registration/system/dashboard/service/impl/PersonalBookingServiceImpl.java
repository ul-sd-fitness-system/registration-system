package com.registration.system.dashboard.service.impl;

import com.registration.system.dashboard.api.ui.BookingUI;
import com.registration.system.dashboard.db.domain.PersonalBooking;
import com.registration.system.dashboard.service.BookedClass;

public class PersonalBookingServiceImpl extends BookedClass{
	
	public PersonalBookingServiceImpl() {
		super("Personal");
		// TODO Auto-generated constructor stub
	}

	public PersonalBooking bookDone(BookingUI freeBookingUI) {
		PersonalBooking booking = new PersonalBooking();
		booking.setName(freeBookingUI.getName());
		booking.setEmail(freeBookingUI.getEmail());
		booking.setLocation(freeBookingUI.getLocation());
		booking.setClassType(freeBookingUI.getClassType());
		booking.setDate(freeBookingUI.getDate());
		return booking;	
	}
	
	@Override
	public void notifySub() {
		System.out.println("Hi");
		
	}

}
