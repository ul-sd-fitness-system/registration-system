package com.registration.system.dashboard.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.registration.system.dashboard.api.ui.InstructorUI;
import com.registration.system.dashboard.db.dao.InstructorDao;
import com.registration.system.dashboard.db.domain.Instructor;
import com.registration.system.dashboard.service.InstructorService;
@Service
public class InstructorServiceImpl implements InstructorService {
	
	@Autowired
	InstructorDao instructorDao;

	@Override
	public void addInstructor(InstructorUI instructorUI) {
		
		Instructor instructor = new Instructor();
		instructor.setInstructorName(instructorUI.getInstructorName());
		instructor.setEmail(instructorUI.getEmail());
		instructor.setPhone(instructorUI.getPhone());
		instructor.setFacilityType(instructorUI.getFacilityType());
		instructorDao.save(instructor);
	}

}
