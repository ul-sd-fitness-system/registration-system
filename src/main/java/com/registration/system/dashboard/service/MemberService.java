package com.registration.system.dashboard.service;

import com.registration.system.dashboard.api.ui.CreateMemberUI;

public interface MemberService {
	
	public void createMember(CreateMemberUI createMemberUI);

}
