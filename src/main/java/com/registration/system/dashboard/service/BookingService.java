package com.registration.system.dashboard.service;

import com.registration.system.dashboard.api.ui.BookingUI;

public interface BookingService {
	
	public void bookingDone(BookingUI bookingUI);

}
