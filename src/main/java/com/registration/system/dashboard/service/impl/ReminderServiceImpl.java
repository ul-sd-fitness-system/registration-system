package com.registration.system.dashboard.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.registration.system.dashboard.db.domain.*;
@Component
@EnableScheduling
public class ReminderServiceImpl {
	
	List<BookedClass> Fbookings = new ArrayList<BookedClass>(); 
	
	public void subscribe(BookedClass obj) {
		Fbookings.add(obj);
	}
	
	public void unsubscribe(BookedClass obj) {
		Fbookings.remove(obj);
	}
	@Scheduled(fixedRate=10000)
	public void reminder() {
		Fbookings.forEach((bookclass)->bookclass.notifySub());
	}

}
