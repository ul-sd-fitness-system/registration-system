package com.registration.system.dashboard.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.registration.system.dashboard.api.ui.FacilityUI;
import com.registration.system.dashboard.db.dao.FacilityDao;
import com.registration.system.dashboard.db.domain.Facility;
import com.registration.system.dashboard.service.CancelService;
import com.registration.system.dashboard.service.FacilityService;

@Service
public class FacilityServiceImpl implements FacilityService, CancelService
{
	
	ReceiverServiceImpl receiver;
	
	@Autowired
	FacilityDao facilityDao;

	@Override
	public void addFacility(FacilityUI facilityUI) {
		Facility facility=new Facility();
		facility.setFacilityType(facilityUI.getFacilityType());
		facilityDao.save(facility);
	}
	
	@Override
	public void delete() {	
		System.out.println("Deleting the details from the database");
		receiver.del();	
		
	}
	
}
