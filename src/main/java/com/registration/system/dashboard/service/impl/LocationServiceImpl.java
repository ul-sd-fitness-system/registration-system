package com.registration.system.dashboard.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.registration.system.dashboard.api.ui.LocationUI;
import com.registration.system.dashboard.db.dao.LocationDao;
import com.registration.system.dashboard.db.domain.Location;
import com.registration.system.dashboard.service.LocationService;

@Service
public class LocationServiceImpl implements LocationService {
	@Autowired
	LocationDao locationDao;

	@Override
	public void addLocation(LocationUI locationUI) {
		Location location = new Location();
		location.setLocationName(locationUI.getLocationName());
		location.setLocationAddress(locationUI.getLocationAddress());
		locationDao.save(location);
	}
	
}
