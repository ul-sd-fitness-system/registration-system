package com.registration.system.dashboard.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.registration.system.dashboard.api.ui.CreateMemberUI;
import com.registration.system.dashboard.db.dao.FitnessMemberDao;
import com.registration.system.dashboard.db.domain.FitnessMember;
import com.registration.system.dashboard.service.MemberService;

@Service
public class MemberServiceImpl implements MemberService{
	
	@Autowired
	FitnessMemberDao fitnessMemberDao;

	@Override
	public void createMember(CreateMemberUI createMemberUI) {
		FitnessMember fitnessMember = new FitnessMember();
		fitnessMember.setName(createMemberUI.getName());
		fitnessMember.setDob(createMemberUI.getDob());
		fitnessMember.setEmail(createMemberUI.getEmail());
		fitnessMember.setPhone(createMemberUI.getPhone());
		fitnessMember.setType(createMemberUI.getType());
		fitnessMember.setMemberType(createMemberUI.getMemberType());
		fitnessMemberDao.save(fitnessMember);
	}

}
