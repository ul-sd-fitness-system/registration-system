package com.registration.system.dashboard.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.registration.system.dashboard.service.impl.ReceiverServiceImpl;

public class InvokerCommandService {
	
	@Autowired
	CancelService cancelservice;
	
	public void deleteButton() {
		System.out.println("Deleting the details from the database");
		cancelservice.delete();
		
	}
}
