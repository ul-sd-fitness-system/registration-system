package com.registration.system.dashboard.service;

import com.registration.system.dashboard.api.ui.InstructorUI;

public interface InstructorService {
	
	public void addInstructor(InstructorUI instructorUI);

}
